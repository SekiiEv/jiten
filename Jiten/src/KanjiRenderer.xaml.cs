﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using System.IO;

namespace Jiten
{
    public partial class KanjiRenderer : UserControl
    {
        public KanjiRenderer()
        {
            InitializeComponent();
            (this.Content as FrameworkElement).DataContext = this;
            this.Loaded += KanjiRenderer_Loaded;
        }

        public void KanjiRenderer_Loaded(object sender, RoutedEventArgs e)
        {
            InlineCollection inlines = KanjiBlock.Inlines;
            inlines.Clear();

            if (MakeLink)
            {
                ToolTip toolTip = new ToolTip();
                toolTip.IsEnabled = true;
                toolTip.Content = RTKMeaning;

                Hyperlink link = new Hyperlink();
                link.ToolTip = toolTip;
                link.Foreground = Brushes.Black;
                link.TextDecorations = null;
                link.PreviewMouseLeftButtonDown += Link_MouseLeftButtonDown;
                KanjiBlock.Inlines.Add(link);
                inlines = link.Inlines;
            }
            
            if (!String.IsNullOrEmpty(KanjiText))
            {
                inlines.Add(KanjiText);
            }
            else
            {
                string filePath;
                if (Kanji.GetImageFilePath(RTKMeaning, out filePath))
                {
                    BitmapImage imageSource = new BitmapImage();
                    FileStream imageStream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite | FileShare.Delete);
                    imageSource.BeginInit();
                    imageSource.StreamSource = imageStream;
                    imageSource.EndInit();

                    Image image = new Image();
                    image.Source = imageSource;
                    image.Height = Size;
                    inlines.Add(image);
                }
            }
        }

        public void Link_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Kanji kanji = new Kanji(RTKNumber, KanjiText, RTKMeaning);
            WindowHandler.OpenWindow(new ViewWindow(kanji), RTKMeaning);
        }

        public void Refresh(string text, string image)
        {
            KanjiText = text;
            KanjiRenderer_Loaded(null, null);
        }

        // Properties
        public double RTKNumber
        {
            get { return (double)this.GetValue(NumberProperty); }
            set { this.SetValue(NumberProperty, value); }
        }
        public readonly static DependencyProperty NumberProperty = DependencyProperty.Register("RTKNumber", typeof(double), typeof(KanjiRenderer), null);

        public string RTKMeaning
        {
            get { return (string)this.GetValue(MeaningProperty); }
            set { this.SetValue(MeaningProperty, value); }
        }
        public readonly static DependencyProperty MeaningProperty = DependencyProperty.Register("RTKMeaning", typeof(string), typeof(KanjiRenderer), null);

        public string KanjiText
        {
            get { return (string)this.GetValue(TextProperty); }
            set { this.SetValue(TextProperty, value); }
        }
        public readonly static DependencyProperty TextProperty = DependencyProperty.Register("KanjiText", typeof(string), typeof(KanjiRenderer), null);

        public string KanjiImage
        {
            get { return (string)this.GetValue(ImageProperty); }
            set { this.SetValue(ImageProperty, value); }
        }
        public readonly static DependencyProperty ImageProperty = DependencyProperty.Register("KanjiImage", typeof(string), typeof(KanjiRenderer), null);

        public double Size { get; set; }

        public bool MakeLink { get; set; }

        public FontFamily KanjiFont
        {
            get { return (FontFamily)this.GetValue(FontProperty); }
            set { this.SetValue(FontProperty, value); }
        }
        public readonly static DependencyProperty FontProperty = DependencyProperty.Register("KanjiFont", typeof(FontFamily), typeof(KanjiRenderer), null);

        // Event to allow user to use bindings with this user control
        public event DependencyPropertyChangedEventHandler PropertyChanged;
        void SetValueDp(DependencyProperty property, object value,
            [System.Runtime.CompilerServices.CallerMemberName] String p = null)
        {
            SetValue(property, value);
            if (PropertyChanged != null)
                PropertyChanged(this, new DependencyPropertyChangedEventArgs());
        }
    }
}
