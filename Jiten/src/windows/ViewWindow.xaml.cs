﻿using System;
using System.Windows;

namespace Jiten
{
    public partial class ViewWindow : Window
    {
        public Kanji[] radicalList   { get; private set; }
        public Kanji[] similarList   { get; private set; }
        public string[] otherMeaningsList { get; private set; }

        public ViewWindow(Kanji kanji)
        {
            InitializeComponent();
            this.DataContext = kanji;

            this.Title = kanji.rtkMeaning;
            if (String.IsNullOrEmpty(kanji.text))
            {
                View.Children.Remove(ComputerFont);
                View.Children.Remove(StrokeOrder);
            }
            else
            {
                this.Title += " " + kanji.text;
            }

            number.Text = "";
            if (kanji.number > 0)
                number.Text = "# " + kanji.number;

            otherMeanings.Text = "";
            otherMeaningsList = Data.Meanings(kanji.rtkMeaning).ToArray();
            if (otherMeaningsList.Length > 0)
            {
                this.otherMeanings.Text = otherMeaningsList[0];
                for (int i = 1; i < otherMeaningsList.Length; ++i)
                {
                    this.otherMeanings.Text += " / " + otherMeaningsList[i];
                }
            }

            radicals.Text = "";
            radicalList = Data.Radicals(kanji.rtkMeaning).ToArray();
            if (radicalList.Length > 0)
            {
                foreach (Kanji radical in radicalList)
                {
                    KanjiRenderer renderer = new KanjiRenderer();
                    renderer.KanjiText = radical.text;
                    renderer.RTKMeaning = radical.rtkMeaning;
                    renderer.RTKNumber = radical.number;
                    renderer.MakeLink = true;
                    renderer.Size = radicals.FontSize;
                    this.radicals.Inlines.Add(renderer);
                    this.radicals.Inlines.LastInline.BaselineAlignment = BaselineAlignment.Center;
                    this.radicals.Inlines.Add(" ");
                }
            }

            mnemonic.Text = "";
            string kanji_mnemonic = Data.Mnemonic(kanji.rtkMeaning);
            if (!String.IsNullOrEmpty(kanji_mnemonic))
            {
                mnemonic.Text = kanji_mnemonic;
            }

            similarList = Data.SimilarKanji(kanji.rtkMeaning).ToArray();
            if (similarList.Length > 0)
            {
                foreach (Kanji similar in similarList)
                {
                    KanjiRenderer renderer = new KanjiRenderer();
                    renderer.RTKNumber = similar.number;
                    renderer.KanjiText = similar.text;
                    renderer.RTKMeaning = similar.rtkMeaning;
                    renderer.RTKNumber = similar.number;
                    renderer.MakeLink = true;
                    renderer.Size = similarKanji.FontSize;
                    this.similarKanji.Inlines.Add(renderer);
                    this.similarKanji.Inlines.LastInline.BaselineAlignment = BaselineAlignment.Center;
                    this.similarKanji.Inlines.Add(" ");
                }
            }
            else
            {
                similarKanji.Text = "";
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            var w = new EditWindow();
            w.SetInitialValues(this);
            w.Grid.Children.Remove(w.KeepAddingButton);
            w.Title = this.Title;
            w.Top  = this.Top  + Math.Abs(this.Height - w.Height)/2;
            w.Left = this.Left + Math.Abs(this.Width  - w.Width )/2;

            WindowHandler.OpenWindow(w, rtkMeaning.Text);
        }
    }
}
