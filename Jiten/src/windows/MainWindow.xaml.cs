﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

using System.Collections.ObjectModel;

namespace Jiten
{
    public partial class MainWindow : Window
    {
        private ObservableCollection<Kanji> KanjiItems;

        public MainWindow()
        {
            InitializeComponent();

            Data.OpenConnection();
            InitList();
        }
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            WindowHandler.CloseAllWindows();
        }

        public void InitList()
        {
            CollectionView cv;
            KanjiItems = new ObservableCollection<Kanji>(Data.GetKanjiList());
            KanjiList.ItemsSource = KanjiItems;
            cv = (CollectionView)CollectionViewSource.GetDefaultView(KanjiList.ItemsSource);
            cv.Filter = KanjiFilter;
        }

        private void AddKanjiButton_Click(object sender, RoutedEventArgs e)
        {
            WindowHandler.OpenBlankEditWindow(new EditWindow());
        }

        public void AddKanji(double number, string kanji, string meaning)
        {
            KanjiItems.Add(new Kanji(number, kanji, meaning));
        }

        private void RemoveKanjiButton_Click(object sender, RoutedEventArgs e)
        {
            int nItems = KanjiList.SelectedItems.Count;
            if (nItems == 0) return;

            Kanji[] items = new Kanji[nItems];
            KanjiList.SelectedItems.CopyTo(items, 0);
            foreach (Kanji kanji in items)
            {
                KanjiItems.Remove(kanji);
                if (String.IsNullOrEmpty(kanji.text))
                    Data.RemoveImage(kanji.rtkMeaning);
            }
            Data.RemoveKanjis(items);
        }

        private bool KanjiFilter(object item)
        {
            if (String.IsNullOrEmpty(KanjiFilterBox.Text))
                return true;

            Kanji kanji = item as Kanji;
            return (!String.IsNullOrEmpty(kanji.text) && (kanji.text.IndexOf(KanjiFilterBox.Text, StringComparison.OrdinalIgnoreCase) >= 0)) ||
                (kanji.rtkMeaning.IndexOf(KanjiFilterBox.Text, StringComparison.OrdinalIgnoreCase) >= 0);
        }

        private void KanjiFilterBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(KanjiList.ItemsSource).Refresh();
        }

        private void KanjiRow_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (KanjiList.SelectedItems.Count == 1)
            {
                Kanji k = KanjiList.SelectedItem as Kanji;
                WindowHandler.OpenWindow(new ViewWindow(k), k.rtkMeaning);
            }
            else
                KanjiList.SelectedIndex = -1;
        }
    }
}
