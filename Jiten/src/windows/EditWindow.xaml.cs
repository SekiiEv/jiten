﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Collections.Generic;

using Microsoft.Win32;
using System.IO;

namespace Jiten
{
    public partial class EditWindow : Window
    {
        private static Brush ERROR_BRUSH = Brushes.OrangeRed;

        // Initial values if any
        private Kanji iKanji = null;
        private string iMnemonic, iKanjiImageFile;
        private string[] iOtherMeanings, iRadicals, iSimilars;

        private ImageBrush kanjiBrush = null;
        private string kanjiImageFile = null;

        public EditWindow()
        {
            InitializeComponent();
        }

        public void SetInitialValues(ViewWindow window)
        {
            iKanji = window.DataContext as Kanji;
            iOtherMeanings = window.otherMeaningsList;
            iRadicals = window.radicalList.Select(k => k.rtkMeaning).ToArray();
            iSimilars = window.similarList.Select(k => k.rtkMeaning).ToArray();
            iMnemonic = window.mnemonic.Text;

            if (iKanji.number > 0)
                NumberTextBox.Text = iKanji.number.ToString();
            MnemonicTextBox.Text = iMnemonic;
            RadicalsTextBox.Text = string.Join(" / ", iRadicals);
            SimilarsTextBox.Text = string.Join(" / ", iSimilars);

            MeaningsTextBox.Text = iKanji.rtkMeaning;
            if (iOtherMeanings != null && iOtherMeanings.Length > 0)
            {
                MeaningsTextBox.Text += " / " + string.Join(" / ", iOtherMeanings);
            }

            if (!String.IsNullOrEmpty(iKanji.text))
            {
                KanjiTextBox.Text = iKanji.text;
            }
            else
            {
                if (iKanji.GetImageFilePath(out iKanjiImageFile))
                {
                    SetImageBrush(iKanjiImageFile);
                }
            }
        }

        private void SetImageBrush(string file)
        {
            BitmapImage imageSource = new BitmapImage();
            FileStream imageStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite | FileShare.Delete);
            imageSource.BeginInit();
            imageSource.StreamSource = imageStream;
            imageSource.EndInit();

            kanjiImageFile = file;
            kanjiBrush = new ImageBrush(imageSource);

            KanjiTextBox.Text = "";
            KanjiTextBox.Background = kanjiBrush;
        }

        private void SelectImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image Files (*.png, *.jpg)|*.png;*.jpg|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                string file = openFileDialog.FileName;

                string extension = System.IO.Path.GetExtension(file).ToLower();
                if (extension.Equals(".png") || extension.Equals(".jpg"))
                {
                    SetImageBrush(file);
                }
            }
        }

        private void KanjiTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (kanjiBrush != null && KanjiTextBox.Text.Length > 0)
            {
                kanjiBrush = null;
                kanjiImageFile = null;
            }
        }
        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        { (sender as TextBox).Background = Brushes.White; }
        private void KanjiTextBox_LostFocus(object sender, RoutedEventArgs e)
        { if (kanjiBrush != null) KanjiTextBox.Background = kanjiBrush; }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            // Parse data and check if it's valid
            string errors = "";

            double rtk_number = -1;
            try
            {
                if (NumberTextBox.Text.Length > 0)
                {
                    rtk_number = double.Parse(NumberTextBox.Text);
                    if (rtk_number <= 0)
                    {
                        errors += "You need to enter a number higher than 0.\n";
                        NumberTextBox.Background = ERROR_BRUSH;
                    }
                    else
                    {
                        NumberTextBox.Background = Brushes.White;
                    }
                }
            }
            catch(Exception)
            {
                errors += "You need to enter a valid number.\n";
                NumberTextBox.Background = ERROR_BRUSH;
            }

            string rtk_meaning = null;
            string[] meanings = null;
            if (String.IsNullOrEmpty(MeaningsTextBox.Text))
            {
                errors += "You need to enter at least one meaning.\n";
                MeaningsTextBox.Background = ERROR_BRUSH;
            }
            else
            {
                meanings = MeaningsTextBox.Text.Split('/');
                rtk_meaning = meanings[0].Trim();
                meanings = meanings.Skip(1).Select(x => x.Trim()).ToArray();
                if ((iKanji == null || iKanji.rtkMeaning != rtk_meaning)
                    && Data.KanjiExists(rtk_meaning))
                {
                    errors += "First meaning must be unique.\n";
                    MeaningsTextBox.Background = ERROR_BRUSH;
                }
                else
                {
                    MeaningsTextBox.Background = Brushes.White;
                }
            }

            if (!String.IsNullOrEmpty(errors))
            {
                MessageBox.Show(this, errors, "Incorrect information", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string[] radicals = RadicalsTextBox.Text.Split('/').Select(x => x.Trim()).Where(x => !String.IsNullOrEmpty(x)).ToArray();
            string[] similars = SimilarsTextBox.Text.Split('/').Select(x => x.Trim()).Where(x => !String.IsNullOrEmpty(x)).ToArray();

            // Update or add kanji. Check initial info to check if we were editing or if changes have been made
            if (iKanji != null)
            {
                if (!rtk_meaning.Equals(iKanji.rtkMeaning))
                {
                    // We were editing the kanji, but now this has a new meaning
                    // If this happens we treat it as a new kanji
                    iKanji = null;
                }
                else
                {
                    bool updateMainWindow = false;
                    if ((iKanji.number != rtk_number) || (iKanji.text != KanjiTextBox.Text) || (iMnemonic != MnemonicTextBox.Text))
                    {
                        Data.UpdateKanji(rtk_number, rtk_meaning, KanjiTextBox.Text, MnemonicTextBox.Text);
                        updateMainWindow = true;
                    }
                    if (KanjiTextBox.Text != null && iKanjiImageFile != kanjiImageFile)
                    {
                        Data.AddImage(rtk_meaning, kanjiImageFile);
                        updateMainWindow = true;
                    }
                    if (updateMainWindow)
                    {
                        MainWindow mainWindow = (MainWindow)Application.Current.MainWindow;
                        mainWindow.InitList();
                    }

                    var meaningsTuple = CheckDifferents(iOtherMeanings, meanings);
                    Data.UpdateMeanings(rtk_meaning, meaningsTuple);

                    bool hasRadicalsChanged = (iRadicals.Length != radicals.Length);
                    if (!hasRadicalsChanged)
                    {
                        for (int i = 0; i < radicals.Length; ++i)
                        {
                            hasRadicalsChanged = !radicals[i].Equals(iRadicals[i]);
                            if (hasRadicalsChanged) break;
                        }
                    }
                    if (hasRadicalsChanged) Data.UpdateRadicals(rtk_meaning, radicals);

                    var similarsTuple = CheckDifferents(iSimilars, similars);
                    Data.UpdateSimilars(rtk_meaning, similarsTuple);
                }
            }

            if (iKanji == null)
            {
                if (!String.IsNullOrEmpty(kanjiImageFile))
                {
                    Data.AddImage(rtk_meaning, kanjiImageFile);
                }

                Data.AddKanji(rtk_number, rtk_meaning, KanjiTextBox.Text, MnemonicTextBox.Text, meanings, radicals, similars);

                MainWindow mainWindow = (MainWindow)Application.Current.MainWindow;
                mainWindow.AddKanji(rtk_number, KanjiTextBox.Text, rtk_meaning);
            }

            // Check if radial button is checked
            if ((KeepAddingButton != null) && (KeepAddingButton.IsChecked ?? false))
            {
                KanjiTextBox.Focus();
                KanjiTextBox   .Text = "";
                MeaningsTextBox.Text = "";
                RadicalsTextBox.Text = "";
                SimilarsTextBox.Text = "";
                MnemonicTextBox.Text = "";
                NumberTextBox  .Text = "";
            }
            else
            {
                var w = new ViewWindow(new Kanji(rtk_number, KanjiTextBox.Text, rtk_meaning));
                w.Top = this.Top + Math.Abs(this.Height - w.Height) / 2;
                w.Left = this.Left + Math.Abs(this.Width - w.Width) / 2;
                WindowHandler.OpenWindow(w, rtk_meaning);
                this.Close();
            }
        }

        private (string[] oldElements, string[] newElements) CheckDifferents (string[] initial, string[] actual)
        {
            bool found;

            var oldElements = new List<string>();
            foreach (string i in initial) {
                found = false;
                foreach (string a in actual) {
                    if (i.Equals(a)) { found = true; break; }
                }
                if (!found) oldElements.Add(i);
            }

            var newElements = new List<string>();
            foreach (string a in actual) {
                found = false;
                foreach (string i in initial) {
                    if (i.Equals(a)) { found = true; break; }
                }
                if (!found) newElements.Add(a);
            }

            return (oldElements.ToArray(), newElements.ToArray());
        }
    }
}