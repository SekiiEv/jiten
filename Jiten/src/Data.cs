﻿using System;
using System.Collections.Generic;

using System.Data.SQLite;
using System.IO;

namespace Jiten
{
    public class Kanji
    {
        public double number { get; set; }
        public string text { get; set; }
        public string rtkMeaning { get; set; }

        public Kanji(object number, object text, object rtkMeaning)
        {
            if (number != null     && number != DBNull.Value)     this.number     = (double)number;
            if (text != null       && text != DBNull.Value)       this.text       = (string)text;
            if (rtkMeaning != null && rtkMeaning != DBNull.Value) this.rtkMeaning = (string)rtkMeaning;
        }

        public bool GetImageFilePath(out string filePath)
        { return Kanji.GetImageFilePath(rtkMeaning, out filePath); }

        public static bool GetImageFilePath(string rtkMeaning, out string filePath)
        {
            filePath = Data.imageFolder + "/" + rtkMeaning + ".png";
            if (File.Exists(filePath))
                return true;

            filePath = Data.imageFolder + "/" + rtkMeaning + ".jpg";
            if (File.Exists(filePath))
                return true;

            filePath = null;
            return false;
        }
    }

    public static class Data
    {
        public const string imageFolder = "Images";

        private const string fileName = "data.sqlite";
        private static SQLiteConnection connection;

        public static void OpenConnection()
        {
            //File.Delete(fileName);
            if (!File.Exists(fileName))
            {
                Console.WriteLine("Database does not exist. Creating one...");
                SQLiteConnection.CreateFile(fileName);

                connection = new SQLiteConnection("Data Source=" + fileName + ";Version=3");
                connection.Open();

                string sql = Properties.Resources.CreateTable;
                SQLiteCommand command = new SQLiteCommand(sql, connection);
                command.ExecuteNonQuery();

                sql = Properties.Resources.InsertTestValues;
                command = new SQLiteCommand(sql, connection);
                command.ExecuteNonQuery();
            }
            else
            {
                connection = new SQLiteConnection("Data Source=" + fileName + ";Version=3");
                connection.Open();
            }
        }

        public static void DeleteAllValues()
        {
            connection.Cancel();
            const string sqlDrop = @"DELETE FROM Kanji;
                                     DELETE FROM Kanji_Radical;
                                     DELETE FROM Kanji_Meaning;
                                     DELETE FROM Kanji_Similar;";

            SQLiteCommand command = new SQLiteCommand(sqlDrop, connection);
            command.ExecuteNonQuery();
        }
        public static void DeleteAllImages()
        {
            DirectoryInfo directory = new DirectoryInfo(imageFolder);
            if (!directory.Exists)
                return;

            foreach (FileInfo image in directory.GetFiles())
            {
                image.Delete();
            }
        }
        public static void InsertTestValues()
        {
            string sql = Properties.Resources.InsertTestValues;
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            command.ExecuteNonQuery();
        }

        public static List<Kanji> GetKanjiList()
        {
            const string sql = "SELECT rtk_number,kanji_text,rtk_meaning FROM Kanji ORDER BY rtk_number ASC";
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            var reader = command.ExecuteReader();

            List<Kanji> list = new List<Kanji>();
            while (reader.Read())
            {
                list.Add(new Kanji(reader["rtk_number"], reader["kanji_text"], reader["rtk_meaning"]));
            }
            return list;
        }

        public static void AddKanji(double number, string rtk_meaning, string kanji, string mnemonic, string[] otherMeanings, string[] radicals, string[] similars)
        {
            const string sql = "INSERT INTO Kanji (rtk_number, rtk_meaning, kanji_text, mnemonic) VALUES (@number, @meaning, @kanji, @mnemonic)";
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            command.Parameters.Add(new SQLiteParameter("@number", number));
            command.Parameters.Add(new SQLiteParameter("@meaning", rtk_meaning));
            command.Parameters.Add(new SQLiteParameter("@kanji", kanji));
            command.Parameters.Add(new SQLiteParameter("@mnemonic", mnemonic));
            command.ExecuteNonQuery();

            if (otherMeanings != null)
            {
                const string sqlMeanings = @"INSERT INTO Kanji_Meaning (kanji, meaning) VALUES (@kanji, @meaning);";
                command = new SQLiteCommand(sqlMeanings, connection);
                command.Parameters.Add(new SQLiteParameter("@kanji", rtk_meaning));
                command.Parameters.Add("@meaning", System.Data.DbType.String);
                foreach (string m in otherMeanings)
                {
                    command.Parameters["@meaning"] = new SQLiteParameter("@meaning", m);
                    command.ExecuteNonQuery();
                }
            }

            if (radicals != null)
            {
                const string sqlRadicals = @"INSERT INTO Kanji_Radical (kanji, radical, position) VALUES (@kanji, @radical, @position);";
                command = new SQLiteCommand(sqlRadicals, connection);
                command.Parameters.Add(new SQLiteParameter("@kanji", rtk_meaning));
                command.Parameters.Add("@radical", System.Data.DbType.String);
                command.Parameters.Add("@position", System.Data.DbType.Int16);
                for (int i = 0; i < radicals.Length; ++i)
                {
                    command.Parameters["@radical"] = new SQLiteParameter("@radical", radicals[i]);
                    command.Parameters["@position"] = new SQLiteParameter("@position", i);
                    command.ExecuteNonQuery();
                }
            }

            if (similars != null)
            {
                const string sqlSimilars = @"INSERT INTO Kanji_Similar (kanji1, kanji2) VALUES (@kanji, @similar);";
                command = new SQLiteCommand(sqlSimilars, connection);
                command.Parameters.Add(new SQLiteParameter("@kanji", rtk_meaning));
                command.Parameters.Add("@similar", System.Data.DbType.String);
                foreach (string similar in similars)
                {
                    command.Parameters["@similar"] = new SQLiteParameter("@similar", similar);
                    command.ExecuteNonQuery();
                }
            }
        }
        public static void AddImage(string rtk_meaning, string imageFile)
        {
            string nameFile = rtk_meaning + System.IO.Path.GetExtension(imageFile).ToLower();

            Directory.CreateDirectory(Data.imageFolder);
            File.Copy(imageFile, Data.imageFolder + '/' + nameFile, true);
        }

        public static void RemoveKanjis(Kanji[] kanji)
        {
            string sql = @"DELETE FROM Kanji WHERE rtk_meaning IN (@@);
                           DELETE FROM Kanji_Radical WHERE kanji IN (@@);
                           DELETE FROM Kanji_Meaning WHERE kanji IN (@@);
                           DELETE FROM Kanji_Similar WHERE kanji1 IN (@@);
                           DELETE FROM Kanji_Similar WHERE kanji2 IN (@@);";
            string parametres = "@meaning0";
            for (int i = 1; i < kanji.Length; ++i)
            {
                parametres += ",@meaning" + i.ToString();
            }
            sql = sql.Replace("@@", parametres);
            SQLiteCommand command = new SQLiteCommand(sql, connection);

            for (int i = 0; i < kanji.Length; ++i)
            {
                command.Parameters.Add(new SQLiteParameter("@meaning" + i.ToString(), kanji[i].rtkMeaning));
            }

            command.ExecuteNonQuery();
        }
        public static void RemoveImage(string rtk_meaning)
        {
            string filePath;
            if (Kanji.GetImageFilePath(rtk_meaning, out filePath))
            {
                File.Delete(filePath);
            }
        }

        public static bool KanjiExists(string rtk_meaning)
        {
            const string sql = @"SELECT COUNT(rtk_meaning) FROM Kanji WHERE rtk_meaning=@meaning";
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            command.Parameters.Add(new SQLiteParameter("@meaning", rtk_meaning));
            long count = (long)command.ExecuteScalar();

            return (count == 0) ? false : true;
        }
        public static List<Kanji> SimilarKanji(string kanji)
        {
            const string sql = @"SELECT rtk_number,kanji_text,rtk_meaning FROM Kanji
                                        WHERE rtk_meaning IN (SELECT kanji1 FROM Kanji_Similar WHERE kanji2=@kanji)
                                 UNION
                                 SELECT rtk_number,kanji_text,rtk_meaning FROM Kanji
                                        WHERE rtk_meaning IN (SELECT kanji2 FROM Kanji_Similar WHERE kanji1=@kanji)";
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            command.Parameters.Add(new SQLiteParameter("@kanji", kanji));
            var reader = command.ExecuteReader();

            List<Kanji> list = new List<Kanji>();
            while (reader.Read())
            {
                list.Add(new Kanji(reader["rtk_number"], reader["kanji_text"], reader["rtk_meaning"]));
            }
            return list;
        }
        public static List<Kanji> Radicals(string kanji)
        {
            const string sql = @"SELECT rtk_number,kanji_text,rtk_meaning
                                 FROM (Kanji INNER JOIN Kanji_Radical ON radical=rtk_meaning)
                                 WHERE Kanji_Radical.kanji=@kanji ORDER BY position ASC";
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            command.Parameters.Add(new SQLiteParameter("@kanji", kanji));
            var reader = command.ExecuteReader();

            List<Kanji> list = new List<Kanji>();
            while (reader.Read())
            {
                list.Add(new Kanji(reader["rtk_number"], reader["kanji_text"], reader["rtk_meaning"]));
            }
            return list;
        }
        public static string Mnemonic(string kanji)
        {
            const string sql = @"SELECT mnemonic FROM Kanji WHERE rtk_meaning=@kanji";
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            command.Parameters.Add(new SQLiteParameter("@kanji", kanji));
            var mnemonic = command.ExecuteScalar();

            return mnemonic as string;
        }
        public static List<string> Meanings(string kanji)
        {
            const string sql = @"SELECT meaning FROM Kanji_Meaning WHERE kanji=@kanji";
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            command.Parameters.Add(new SQLiteParameter("@kanji", kanji));
            var reader = command.ExecuteReader();

            List<string> list = new List<string>();
            while (reader.Read())
            {
                list.Add(reader["meaning"] as string);
            }
            return list;
        }

        public static void UpdateKanji(double rtk_number, string rtk_meaning, string kanji, string mnemonic)
        {
            const string sql = "UPDATE Kanji SET rtk_number = @number, kanji_text = @kanji, mnemonic = @mnemonic WHERE rtk_meaning = @meaning";
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            command.Parameters.Add(new SQLiteParameter("@number", rtk_number));
            command.Parameters.Add(new SQLiteParameter("@meaning", rtk_meaning));
            command.Parameters.Add(new SQLiteParameter("@kanji", kanji));
            command.Parameters.Add(new SQLiteParameter("@mnemonic", mnemonic));
            command.ExecuteNonQuery();
        }
        public static void UpdateMeanings(string rtk_meaning, (string[] oldElements, string[] newElements) tuple)
        {
            const string DeleteSQL = "DELETE FROM Kanji_Meaning WHERE kanji=@meaning AND meaning IN (@@);";
            const string InsertSQL = "INSERT INTO Kanji_Meaning (kanji, meaning) VALUES @@;";
            UpdateWrapper(rtk_meaning, tuple, DeleteSQL, InsertSQL);
        }
        public static void UpdateRadicals(string rtk_meaning, string[] radicals)
        {
            string sql = @"DELETE FROM Kanji_Radical WHERE kanji=@meaning;";

            if (radicals.Length > 0)
            {
                sql += "INSERT INTO Kanji_Radical (kanji, radical, position) VALUES @@;";

                string parametres = "(@meaning, @radical0, @position0)";
                for (int i = 1; i < radicals.Length; ++i)
                {
                    parametres += ",(@meaning, @radical" + i.ToString() + ", @position" + i.ToString() + ")";
                }
                sql = sql.Replace("@@", parametres);
            }

            SQLiteCommand command = new SQLiteCommand(sql, connection);
            command.Parameters.Add(new SQLiteParameter("@meaning", rtk_meaning));


            for (int i = 0; i < radicals.Length; ++i)
            {
                command.Parameters.Add(new SQLiteParameter("@radical"  + i.ToString(), radicals[i]));
                command.Parameters.Add(new SQLiteParameter("@position" + i.ToString(), i));
            }

            command.ExecuteNonQuery();
        }
        public static void UpdateSimilars(string rtk_meaning, (string[] oldElements, string[] newElements) tuple)
        {
            const string DeleteSQL = @"DELETE FROM Kanji_Similar WHERE kanji1 IN (@@) AND kanji2=@meaning;
                                       DELETE FROM Kanji_Similar WHERE kanji2 IN (@@) AND kanji1=@meaning;";
            const string InsertSQL = "INSERT INTO Kanji_Similar (kanji1, kanji2) VALUES @@;";
            UpdateWrapper(rtk_meaning, tuple, DeleteSQL, InsertSQL);
        }
        private static void UpdateWrapper(string rtk_meaning, (string[] oldElements, string[] newElements) tuple, string DeleteSQL, string InsertSQL)
        {
            string sql = "";

            if (tuple.oldElements.Length > 0)
            {
                sql += DeleteSQL;

                string parametres = "@meaningA0";
                for (int i = 1; i < tuple.oldElements.Length; ++i)
                {
                    parametres += ",@meaningA" + i.ToString();
                }
                sql = sql.Replace("@@", parametres);
            }

            if (tuple.newElements.Length > 0)
            {
                sql += InsertSQL;

                string parametres = "(@meaning, @meaningB0)";
                for (int i = 1; i < tuple.newElements.Length; ++i)
                {
                    parametres += ",(@meaning, @meaningB" + i.ToString() + ")";
                }
                sql = sql.Replace("@@", parametres);
            }

            SQLiteCommand command = new SQLiteCommand(sql, connection);

            command.Parameters.Add(new SQLiteParameter("@meaning", rtk_meaning));
            for (int i = 0; i < tuple.oldElements.Length; ++i)
            {
                command.Parameters.Add(new SQLiteParameter("@meaningA" + i.ToString(), tuple.oldElements[i]));
            }
            for (int i = 0; i < tuple.newElements.Length; ++i)
            {
                command.Parameters.Add(new SQLiteParameter("@meaningB" + i.ToString(), tuple.newElements[i]));
            }

            command.ExecuteNonQuery();
        }
    }
}
