﻿using System.Collections.Generic;
using System.Windows;

namespace Jiten
{
    public static class WindowHandler
    {
        private static Window blankEditWindow = null;
        private static Dictionary<string, Window> windows;
        
        static WindowHandler ()
        {
            windows = new Dictionary<string, Window>();
        }

        public static void OpenBlankEditWindow(EditWindow window)
        {
            if (blankEditWindow != null) blankEditWindow.Close();
            blankEditWindow = window;
            blankEditWindow.Show();
        }

        public static void OpenWindow(Window window, string kanji)
        {
            Window existingWindow;
            if (windows.TryGetValue(kanji, out existingWindow))
            {
                existingWindow.Close();
                windows.Remove(kanji);
            }

            windows.Add(kanji, window);
            window.Show();
        }

        public static void CloseAllWindows()
        {
            if (blankEditWindow != null)
            {
                blankEditWindow.Close();
                blankEditWindow = null;
            }

            foreach (Window w in windows.Values)
            {
                w.Close();
            }
            windows.Clear();
        }
    }
}
