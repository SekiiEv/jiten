﻿using System.Windows;
using System.Windows.Input;

namespace Jiten
{
    public static class CustomCommands
    {
        public static readonly RoutedUICommand ClearValues = new RoutedUICommand
        (
            "Clear database",
            "Creal all values from the database",
            typeof(CustomCommands),
            null
        );

        public static readonly RoutedUICommand ResetValues = new RoutedUICommand
        (
            "Insert test values",
            "Reset values to the initial ones",
            typeof(CustomCommands),
            null
        );

        public static readonly RoutedUICommand Exit = new RoutedUICommand
        (
            "Exit",
            "Exit Application",
            typeof(CustomCommands),
            new InputGestureCollection()
            {
                    new KeyGesture(Key.F4, ModifierKeys.Alt)
            }
        );
    }

    public partial class MainWindow : Window
    {
        private void CanExecuteAlwaysTrue(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ClearValues_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            WindowHandler.CloseAllWindows();
            Data.DeleteAllValues();
            Data.DeleteAllImages();
            InitList();
        }

        private void ResetValues_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            WindowHandler.CloseAllWindows();
            Data.DeleteAllValues();
            Data.DeleteAllImages();
            Data.InsertTestValues();
            InitList();
        }


        private void Exit_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
