﻿-- Values to test several aspects of the application

INSERT INTO Kanji (rtk_number, rtk_meaning, kanji_text, mnemonic) VALUES
                  (1587,       'now',       '今',       NULL),
				  (12,         'day',       '日',       NULL),
				  (13,         'month',     '月',       NULL),
				  (20,         'bright',    '明',       'SUN gives BRIGHTNESS to the MOON'),
				  (19,         'companion', '朋',       NULL),
				  (51.1,       'mist',      '𠦝',       NULL),
                  (52,         'morning',   '朝',       NULL),
                  (838,        'husband',   '夫',       NULL),
                  (216,        'not yet',   '未',       NULL),
                  (428,        'heavens',   '天',       NULL),
                  (35.1,       'dot',       '丶',       NULL),
                  (37,         'white',     '白',       'a WHITE ray is a DROP of SUN'),
                  (15,         'eye',       '目',       NULL),
                  (36,         'oneself',   '自',       NULL);
INSERT INTO Kanji_Radical (kanji,       radical, position) VALUES
						  ('bright',    'day',   0),
						  ('bright',    'month', 1),
						  ('companion', 'month', 0),
						  ('companion', 'month', 1),
						  ('morning',   'mist',  0),
						  ('morning',   'month', 1),
						  ('white',     'dot',   0),
						  ('white',     'day',   1),
						  ('oneself',   'dot',   0),
						  ('oneself',   'eye',   1);
INSERT INTO Kanji_Meaning (kanji,     meaning) VALUES
						  ('now',     'clock'),
						  ('day',     'sun'),
						  ('month',   'moon'),
						  ('month',   'flesh'),
						  ('dot',     'drop'),
						  ('white',   'dove');
INSERT INTO Kanji_Similar (kanji1,    kanji2) VALUES
                          ('not yet', 'husband'),
                          ('not yet', 'heavens'),
                          ('heavens', 'husband'),
                          ('white',   'oneself');
