﻿-- Creates all the tables needed to manage the application. It may
-- contain extra fields that are intended for future use.

CREATE TABLE Kanji (
	rtk_number FLOAT,
	rtk_meaning VARCHAR UNIQUE PRIMARY KEY,
	kanji_text CHAR(1),
	mnemonic VARCHAR
);
CREATE TABLE Kanji_Radical ( -- 1..n
	kanji VARCHAR,
	radical VARCHAR,
	position TINYINT
);
CREATE TABLE Kanji_Meaning ( -- 1..n
	kanji VARCHAR,
	meaning VARCHAR
);
CREATE TABLE Kanji_Similar ( -- n..n symmetrical
	kanji1 VARCHAR,
	kanji2 VARCHAR
);
