# Jiten - Personal Dictionary

Windows Application to store Japanese Kanji with own meanings and mnemonics.
The user interface has been built using **C#** and the **WPF** graphical subsystem and it's connected to an internal database using **SQLite**.

![](git-img/main_window.png "Main Window listing all the Kanji.")

The user can perform basic actions like **adding and deleting** kanji as well as **searching** a specific kanji using the search box.
The user can also double click on a Kanji to open a more **detailed view** of it or **edit** the information.

Edit Window | View window
:-------------------------:|:-------------------------:
![](git-img/edit.png "Kanji edition window.") | ![](git-img/view.png "Kanji detailed view window.")

The detailed view shows the kanji with its main meaning, the radicals (distinct parts) of which it is formed and other kanji that might lead to confusion.

Smallers radicals might be group together into a bigger one so they are easier to remember.
Because of this, a text version of these does not exist, so an image is provided and rendered as if it was text.
For example, the kanji "by one's side" on the first image is not really a Kanji and a photo of it has been provided.

## Downloads

Download the [portable version](/builds/jiten_portable.7z) of the application for Windows.